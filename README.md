## Poridhi Assignment - 2
***Note: I tested the commands below using root account. You may need to add "sudo" if you're not root user.***

* Create the namespaces
```bash
ip netns add red
ip netns add green
```

* Create the interfaces for namespaces and their veth peers
```bash
ip link add red-veth type veth peer name red-veth-peer
ip link add green-veth type veth peer name green-veth-peer
```

* Connect the interfaces to their namespaces
```bash
ip link set web-veth netns web
ip link set db-veth netns database
```

* Create a network bridge and connect the veth peers with this bridge
```bash
ip link add app-br type bridge
ip link set red-veth-peer master app-br
ip link set green-veth-peer master app-br
```

* Set IP addresses to each interfaces
```bash
ip netns exec red ip addr add 10.10.0.10/16 dev red_veth
ip netns exec green ip addr add 10.10.0.20/16 dev green_veth
ip addr add 10.10.0.30/16 dev app_br
```

* Set the default route for namespaces to the bridge IP
```bash
ip netns exec red ip route add default via 10.10.0.30
ip netns exec green ip route add default via 10.10.0.30
```

* Enable/UP the interfaces
```bash
ip link set app-br up
ip link set red-veth-peer up
ip link set green-veth-peer up
ip netns exec red ip link set dev lo up
ip netns exec red ip link set dev red-veth up
ip netns exec green ip link set dev lo up
ip netns exec green ip link set dev green-veth up
```

* Ping to googles' dns
```bash
ping 8.8.8.8
```

* If it still does not work, add the iptable config below, then try again
```bash
iptables --table nat -A POSTROUTING -s 10.10.0.0/16 ! -o app_br -j MASQUERADE
```